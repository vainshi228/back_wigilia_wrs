import knex from 'knex'
import config from "./knexfile";
import Knex from "knex";

export class DatabaseService {
    knex: Knex<any, unknown[]>;
    private connected: boolean = false;
    private stopped: boolean = false;

    constructor() {
        this.knex = knex(config)
    }

    async start() {
        while (!this.stopped && !this.connected) {
            try {
                await this.knex.raw('SELECT 1')
                this.connected = true
            } catch (err) {
                await new Promise((resolve => setTimeout(resolve, 2000)))
            }
        }
    }

    async stop() {
        await this.knex.destroy()
        this.connected = false
        this.stopped = true
    }

}
