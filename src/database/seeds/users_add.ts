import * as Knex from "knex";
import nameEmailUsers from './users.json';
import {User} from "../../endpoints/types";



export async function seed(knex: Knex): Promise<void> {
    // Inserts seed entries
    await knex("users")
        .update({deleted_at: new Date()})
        .where("deleted_at", "is", null)

    let users: User[] = [];

    // @ts-ignore
    for (let user of nameEmailUsers){
        for(let name of Object.keys(user)){
            users.push({
                name: name,
                // @ts-ignore
                email: user[name],
                created_at: new Date(),
                updated_at: new Date()
            } as User)
        }
    }
    await knex("users").insert(users);
};
