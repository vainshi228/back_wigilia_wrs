import {Config} from "knex";
import * as bcrypt from "bcrypt";

const config: Config = {
    client: "postgresql",
    debug: true,
    connection: {
        database: process.env.POSTGRES_DB || "secret_santa",
        user: process.env.POSTGRES_USER || "user",
        password: process.env.POSTGRES_PASSWORD || "user",
        host: process.env.POSTGRES_HOST || "localhost",
        port: parseInt(process.env.POSTGRES_PORT || "5432")
    },
    migrations: {
        directory: __dirname + '/migrations',
        extension: 'ts'
    },
    seeds: {
        directory: __dirname + '/seeds',
        extension: 'ts'
    },
    pool: {
        min: 2,
        max: 10
    }
}

export default config
