import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('messages', (table) => {
        table.increments();
        table.string('message', 300).notNullable();
        table.integer('sender_id');
        table.foreign('sender_id').references('users.id');
        table.boolean('is_private');
        table.integer('receiver_id');
        table.foreign('receiver_id').references('users.id');
        table.timestamp('created_at').notNullable();
        table.timestamp('updated_at').notNullable();
        table.timestamp('deleted_at');
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('messages');
}

