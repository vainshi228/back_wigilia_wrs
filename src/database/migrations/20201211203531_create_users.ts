import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('users', (table) => {
        table.increments();
        table.string('write_link');
        table.string('email').notNullable().unique();
        table.string('name').notNullable();
        table.timestamp('created_at').notNullable();
        table.timestamp('updated_at').notNullable();
        table.timestamp('deleted_at');
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('users');
}

