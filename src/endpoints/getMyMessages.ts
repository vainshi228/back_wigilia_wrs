import {Express} from 'express'
import {Message} from "./types";
import {DatabaseService} from "../database/database-service";
import {validateMember} from "./utils";
import {updateMessage} from "./updateMessage";
import { START_READING } from './constants';

async function getMyMessagesDataBase(database: DatabaseService, userId: number): Promise<Message[]> {
    let messages = await database.knex
        .select<Message[]>('messages.message', 'users.email as receiver_email', "users.name as receiver_name")
        .from('messages')
        .fullOuterJoin('users', function() {
            this.on("users.id", "=", "messages.receiver_id")
        })
        .where('messages.sender_id',  userId)
        .andWhere('messages.deleted_at', 'is', null)
        .andWhere('users.deleted_at', 'is', null);
    messages.forEach((message) => {
        if (message.receiver_name){
            message.is_private
        } else {
            message.receiver_name = undefined
            message.receiver_email = undefined
        }
    })
    return messages;
}

export function getMyMessages(app: Express, database: DatabaseService) {
    app.post('/api/getMyMessages', async function (req, res) {
        if (START_READING > new Date()) {
            await validateMember(req.body, database).then(async (userId) => {
                if (userId){
                    await getMyMessagesDataBase(database, userId)
                        .then((messages) => {
                            res.send(messages).status(200);
                        })
                        .catch((err) => {
                            res.sendStatus(400);
                        })
                } else res.sendStatus(400);
            }).catch((err) => {
                res.sendStatus(500);
            });
        } else {
            res.sendStatus(404);
        }
    })
}
