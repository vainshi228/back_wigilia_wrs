import {CreateUpdateRequest, Members, User} from "./types";
import {DatabaseService} from "../database/database-service";
import * as bcrypt from "bcrypt"
import * as nodemailer from "nodemailer";
import {hashSync} from "bcrypt";
import * as google from "googleapis"
import * as fs from 'fs';

export async function validateUpdateCreate(obj: CreateUpdateRequest, database: DatabaseService): Promise<User[]>{
    return await validateDelete(obj, database).then((users) => {
        if (users.length != 0) {
            if (!(obj.message && obj.message.length < 301))
                return [];
            return users;
        } else return [];
    }).catch(() => {
            return [];
    });

}

export async function validateDelete(obj: CreateUpdateRequest, database: DatabaseService): Promise<User[]>{
    let users: User[] = [];
    let sender : User[] = await database.knex
        .select<User[]>('write_link', 'email', 'id')
        .from('users')
        .where('email', obj.sender_email)
        .andWhere('deleted_at', 'is', null)
        .limit(1);
    if (sender.length == 1 && !(await bcrypt.compare(obj.uniq_code, sender[0].write_link!))) return [];
    if (sender[0])
        users.push(sender[0]);
    else
        return [];
    if (obj.is_private){
        if (obj.receiver_email == obj.sender_email) return [];
        let receiver: User[] = await database.knex
            .select<User[]>('email', 'id')
            .from('users')
            .where('email', obj.receiver_email)
            .andWhere('deleted_at', 'is', null)
            .limit(1);
        if (receiver[0])
            users.push(receiver[0]);
        else
            return [];
    }
    return users;
}

export async function validateRead(database: DatabaseService, uniq_code: string): Promise<number | null>{
    let result = await database.knex
        .select<User[]>('write_link', 'id')
        .from('users')
        .where('deleted_at', 'is', null)
    for (let user of result)
        if (await bcrypt.compare(uniq_code, user.write_link!))
            return user.id!;
    return null;
}

export async function validateMember(obj: Members, database: DatabaseService): Promise<number | null>{
    let sender : User[] = await database.knex
        .select<User[]>('write_link', 'email', 'id')
        .from('users')
        .where('email', obj.email)
        .andWhere('deleted_at', 'is', null)
        .limit(1);
    if (sender.length == 1 && !(await bcrypt.compare(obj.uniq_code, sender[0].write_link!))) return null;
    if (sender[0])
        return sender[0].id!;
    else
        return null;
}

function randomId(): string {
    return ((Math.random().toString(36).substr(2, 5) + Date.now().toString(36) + Math.random().toString(36).substr(2, 5)));
}

const oAuth2Client = new google.google.auth.OAuth2(
    process.env.EMAIL_CLIENT_ID,
    process.env.EMAIL_CLIENT_SECRET,
    'https://developers.google.com/oauthplayground')
oAuth2Client.setCredentials({
    refresh_token: process.env.EMAIL_REFRESH_TOKEN
});

export async function send_invite_mails(date: Date, database: DatabaseService){
    try {
        const accessToken = await oAuth2Client.getAccessToken();
        let url: string = "";
        const mail = fs.readFileSync('msg','utf8');
        const mailHtml = fs.readFileSync('msg_html','utf8');
        const transport = nodemailer.createTransport({
            // @ts-ignore
            service: "gmail",
            auth: {
                type: "OAuth2",
                user: process.env.EMAIL,
                clientId: process.env.EMAIL_CLIENT_ID,
                clientSecret: process.env.EMAIL_CLIENT_SECRET,
                refreshToken: process.env.EMAIL_REFRESH_TOKEN,
                accessToken: accessToken,
            }
        })
        let mails = await database.knex
            .select<User[]>("email", "id", "name")
            .from('users')
            .where("deleted_at", "is", null);
        url = process.env.SERVER_PATH as string;
        for (let user of mails) {
            let pass = randomId()
            let mailOptions = {
                from: process.env.EMAIL,
                to: user.email,
                subject: 'Wigilia WRS FTIMS',
                text: mail.replace("<!---->", url),
                html: mailHtml.replace("<!---->", url + "?uniq_code=" + pass + "&email=" + user.email).replace("<!----->", url),
            };

            await transport.sendMail(mailOptions);

            await database.knex("users")
                .update({
                    //write_link: await hashSync(pass, 6)
                    write_link: await hashSync(pass, 6)
                })
                .where('id', user.id)
        }
    } catch (exc){
        console.log(exc);
    }
}