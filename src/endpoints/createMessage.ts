import {Response, Request, Express} from 'express'
import {User, Message} from "./types";
import {DatabaseService} from "../database/database-service";
import {validateUpdateCreate} from "./utils";
import {START_READING} from "./constants";

async function createMessageDataBase(database: DatabaseService, message: string, sender: number, receiver?: number): Promise<boolean> {
    if (receiver){
        const isRelationships = await database.knex
            .select<Message[]>('id')
            .from('messages')
            .where('sender_id', sender)
            .andWhere('deleted_at', 'is', null)
            .andWhere('receiver_id', receiver)
        if (isRelationships.length == 0) {
            const result = await database.knex('messages')
                .insert( {
                    message: message,
                    sender_id: sender,
                    receiver_id: receiver,
                    created_at: new Date(),
                    updated_at: new Date(),
                });
        } else return false;
    } else {
        const isRelationships = await database.knex
            .select<Message[]>('id')
            .from('messages')
            .where('sender_id', sender)
            .andWhere('receiver_id', 'is', null)
            .andWhere('deleted_at', 'is', null)
        if (isRelationships.length == 0) {
            const result = await database.knex('messages')
                .insert({
                    message: message,
                    sender_id: sender,
                    created_at: new Date(),
                    updated_at: new Date(),
                });
        } else return false;
    }
    return true;
}

export function createMessage(app: Express, database: DatabaseService) {
        app.post('/api/createMessage', async function (req, res) {
            if (START_READING > new Date()) {
                await validateUpdateCreate(req.body, database).then(async (users) => {
                    if (users.length != 0) {
                        await createMessageDataBase(database, req.body.message, users[0].id!, users[1] ? users[1].id : undefined)
                            .then((isAdded) => {
                                if (isAdded) res.sendStatus(200);
                                else res.sendStatus(400)
                            })
                            .catch(() => {
                                res.sendStatus(400);
                            })
                    } else res.sendStatus(400);
                }).catch(() => {
                    res.sendStatus(500);
                });
            } else {
                res.sendStatus(404);
            }
        })

}
