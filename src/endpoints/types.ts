interface BaseType{
    id?: number,
    created_at?: string | Date,
    updated_at?: string | Date,
    deleted_at?: string | Date
}

export interface User extends BaseType{
    write_link?: string,
    name?: string,
    email: string
}

export interface Message extends BaseType{
    message: string,
    sender_id: number,
    is_private: boolean,
    receiver_id?: number,
    receiver_email?: string,
    receiver_name?: string,
}

export interface CreateUpdateRequest {
    message: string,
    sender_email: string,
    is_private: boolean,
    receiver_email?: string,
    uniq_code: string,
}

export interface Members {
    uniq_code?: string;
    email: string;
    name?: string;
}
