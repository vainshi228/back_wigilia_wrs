import {Response, Request, Express} from 'express'
import {User, Message} from "./types";
import {DatabaseService} from "../database/database-service";
import {validateDelete, validateUpdateCreate} from "./utils";
import {START_READING} from "./constants";

async function deleteMessageDataBase(database: DatabaseService, message: string, sender: number, receiver?: number): Promise<boolean> {
    let isRelationships;
    if (receiver){
        isRelationships = await database.knex
            .select<Message[]>('id')
            .from('messages')
            .where('sender_id', sender)
            .andWhere('deleted_at', 'is', null)
            .andWhere('receiver_id', receiver)
    } else {
        isRelationships = await database.knex
            .select<Message[]>('id')
            .from('messages')
            .where('sender_id', sender)
            .andWhere('receiver_id', 'is', null)
            .andWhere('deleted_at', 'is', null)
    }
    if (isRelationships.length == 1) {
        await database.knex('messages')
            .update( {
                updated_at: new Date(),
                deleted_at: new Date(),
            })
            .where("id", isRelationships[0].id);
    } else return false;
    return true;
}

export function deleteMessage(app: Express, database: DatabaseService) {
    app.post('/api/deleteMessage', async function (req, res) {
        if (START_READING > new Date()) {
            await validateDelete(req.body, database).then(async (users) => {
                if (users.length != 0){
                    await deleteMessageDataBase(database, req.body.message, users[0].id!, users[1] ? users[1].id : undefined)
                        .then((isDeleted) => {
                            if (isDeleted) res.sendStatus(200);
                            else res.sendStatus(400)
                        })
                        .catch(() => {
                            res.sendStatus(400);
                        })
                } else res.sendStatus(400);
            }).catch(() => {
                res.sendStatus(500);
            });
        } else {
            res.sendStatus(404);
        }
    })
}
