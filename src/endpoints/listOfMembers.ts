import {Express} from 'express'
import {Members, Message} from "./types";
import {DatabaseService} from "../database/database-service";
import {validateMember} from "./utils";
import { START_READING } from './constants';

async function listOfMembersDataBase(database: DatabaseService, userId: number): Promise<Members[]> {
    let members = await database.knex
        .select<Members[]>('name', 'email')
        .from('users')
        .where('id', '!=', userId)
        .andWhere('deleted_at', 'is', null);
    return members;
}

export function listOfMembers(app: Express, database: DatabaseService) {
    app.post('/api/listOfMembers', async function (req, res) {
        if (START_READING > new Date()) {
            await validateMember(req.body, database).then(async (userId) => {
                if (userId){
                    await listOfMembersDataBase(database, userId)
                        .then((members) => {
                            res.send(members).status(200);
                        })
                        .catch(() => {
                            res.sendStatus(400);
                        })
                } else res.sendStatus(400);
            }).catch((err) => {
                res.sendStatus(500);
            });
        } else {
            res.sendStatus(404);
        }
    })
}
