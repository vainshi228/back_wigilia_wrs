import {Express} from 'express'
import {Message} from "./types";
import {DatabaseService} from "../database/database-service";
import {validateRead} from "./utils";
import { START_READING } from './constants';

async function readMessageDataBase(database: DatabaseService, userId: number): Promise<Message[]> {
    let messages = await database.knex
            .select<Message[]>('message')
            .from('messages')
            .where('deleted_at', 'is', null)
            .andWhere('receiver_id', 'is', null);

    messages.forEach((message) => {message.is_private = false});

    let private_messages = await database.knex
        .select<Message[]>('message')
        .from('messages')
        .where('deleted_at', 'is', null)
        .andWhere('receiver_id', userId);

    private_messages.forEach((message) => {message.is_private = true});

    return messages.concat(private_messages);
}

export function readMessage(app: Express, database: DatabaseService) {
    app.post('/api/readMessage', async function (req, res) {
        if (START_READING < new Date()) {
            await validateRead(database, req.body.uniq_code).then(async (userId) => {
                if (userId) {
                    await readMessageDataBase(database, userId)
                        .then((messages) => {
                            res.send(messages).status(200);
                        })
                        .catch(() => {
                            res.sendStatus(400);
                        })
                } else res.sendStatus(400);
            }).catch((err) => {
                res.sendStatus(500);
            });
        } else res.sendStatus(404);
    })
}
