import {createMessage} from "./createMessage";
import {readMessage} from "./readMessages";
import {updateMessage} from "./updateMessage";
import {deleteMessage} from "./deleteMessage";
import {listOfMembers} from "./listOfMembers";
import {getMyMessages} from "./getMyMessages";

export default {
    getMyMessages,
    listOfMembers,
    createMessage,
    readMessage,
    updateMessage,
    deleteMessage
}
