import * as dotenv from "dotenv";


import express from 'express';
import endpoints from './endpoints';
import {DatabaseService} from "./database/database-service";
import { send_invite_mails } from './endpoints/utils';
// rest of the code remains same

const app = express();
const PORT = 8000;
let cors = require('cors')
app.use(cors())
let database = new DatabaseService();

app.use(express.json());

Object.values(endpoints).forEach((endpoint) => endpoint(app, database))

app.listen(PORT, async () => {
    await send_invite_mails(new Date(), database).catch(e => { console.log(e) });
    console.log(`⚡️[server]: Server is running at port: ${PORT}`);
});

