FROM node:12.17.0-alpine
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json .
COPY . .
RUN npm install
# By default run entrypoint.sh, but if command-line arguments
# are given run those instead:
EXPOSE 8000
ENTRYPOINT ["./entrypoint.sh"]
#ENTRYPOINT ["sh","./entrypoint.sh"]
# RUN npm run migrate
# FROM node:12.17.0-alpine
# CMD npm run start

