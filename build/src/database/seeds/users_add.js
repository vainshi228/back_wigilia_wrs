"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
const users_json_1 = __importDefault(require("./users.json"));
function seed(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        // Inserts seed entries
        yield knex("users")
            .update({ deleted_at: new Date() })
            .where("deleted_at", "is", null);
        let users = [];
        // @ts-ignore
        for (let user of users_json_1.default) {
            for (let name of Object.keys(user)) {
                users.push({
                    name: name,
                    // @ts-ignore
                    email: user[name],
                    created_at: new Date(),
                    updated_at: new Date()
                });
            }
        }
        yield knex("users").insert(users);
    });
}
exports.seed = seed;
;
