"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config = {
    client: "postgresql",
    debug: true,
    connection: {
        database: process.env.POSTGRES_DB || "secret_santa",
        user: process.env.POSTGRES_USER || "user",
        password: process.env.POSTGRES_PASSWORD || "user",
        host: process.env.POSTGRES_HOST || "127.0.0.1",
        port: parseInt(process.env.POSTGRES_PORT || "5434")
    },
    migrations: {
        directory: __dirname + '/migrations',
        extension: 'ts'
    },
    seeds: {
        directory: __dirname + '/seeds',
        extension: 'ts'
    },
    pool: {
        min: 2,
        max: 10
    }
};
exports.default = config;
