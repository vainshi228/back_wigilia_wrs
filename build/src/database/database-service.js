"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseService = void 0;
const knex_1 = __importDefault(require("knex"));
const knexfile_1 = __importDefault(require("./knexfile"));
class DatabaseService {
    constructor() {
        this.connected = false;
        this.stopped = false;
        this.knex = knex_1.default(knexfile_1.default);
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            while (!this.stopped && !this.connected) {
                try {
                    yield this.knex.raw('SELECT 1');
                    this.connected = true;
                }
                catch (err) {
                    yield new Promise((resolve => setTimeout(resolve, 2000)));
                }
            }
        });
    }
    stop() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.knex.destroy();
            this.connected = false;
            this.stopped = true;
        });
    }
}
exports.DatabaseService = DatabaseService;
