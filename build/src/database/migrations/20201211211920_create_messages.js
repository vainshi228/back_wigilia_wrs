"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
function up(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        yield knex.schema.createTable('messages', (table) => {
            table.increments();
            table.string('message', 300).notNullable();
            table.integer('sender_id');
            table.foreign('sender_id').references('users.id');
            table.boolean('is_private');
            table.integer('receiver_id');
            table.foreign('receiver_id').references('users.id');
            table.timestamp('created_at').notNullable();
            table.timestamp('updated_at').notNullable();
            table.timestamp('deleted_at');
        });
    });
}
exports.up = up;
function down(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        yield knex.schema.dropTable('messages');
    });
}
exports.down = down;
