"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.send_invite_mails = exports.validateMember = exports.validateRead = exports.validateDelete = exports.validateUpdateCreate = void 0;
const bcrypt = __importStar(require("bcrypt"));
const nodemailer = __importStar(require("nodemailer"));
const bcrypt_1 = require("bcrypt");
const google = __importStar(require("googleapis"));
const fs = __importStar(require("fs"));
function validateUpdateCreate(obj, database) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield validateDelete(obj, database).then((users) => {
            if (users.length != 0) {
                if (!(obj.message && obj.message.length < 301))
                    return [];
                return users;
            }
            else
                return [];
        }).catch(() => {
            return [];
        });
    });
}
exports.validateUpdateCreate = validateUpdateCreate;
function validateDelete(obj, database) {
    return __awaiter(this, void 0, void 0, function* () {
        let users = [];
        let sender = yield database.knex
            .select('write_link', 'email', 'id')
            .from('users')
            .where('email', obj.sender_email)
            .andWhere('deleted_at', 'is', null)
            .limit(1);
        if (sender.length == 1 && !(yield bcrypt.compare(obj.uniq_code, sender[0].write_link)))
            return [];
        if (sender[0])
            users.push(sender[0]);
        else
            return [];
        if (obj.is_private) {
            if (obj.receiver_email == obj.sender_email)
                return [];
            let receiver = yield database.knex
                .select('email', 'id')
                .from('users')
                .where('email', obj.receiver_email)
                .andWhere('deleted_at', 'is', null)
                .limit(1);
            if (receiver[0])
                users.push(receiver[0]);
            else
                return [];
        }
        return users;
    });
}
exports.validateDelete = validateDelete;
function validateRead(database, uniq_code) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = yield database.knex
            .select('write_link', 'id')
            .from('users')
            .where('deleted_at', 'is', null)
            .limit(1);
        if (result.length == 1 && !(yield bcrypt.compare(uniq_code, result[0].write_link)))
            return null;
        return result[0] ? result[0].id : null;
    });
}
exports.validateRead = validateRead;
function validateMember(obj, database) {
    return __awaiter(this, void 0, void 0, function* () {
        let sender = yield database.knex
            .select('write_link', 'email', 'id')
            .from('users')
            .where('email', obj.email)
            .andWhere('deleted_at', 'is', null)
            .limit(1);
        if (sender.length == 1 && !(yield bcrypt.compare(obj.uniq_code, sender[0].write_link)))
            return null;
        if (sender[0])
            return sender[0].id;
        else
            return null;
    });
}
exports.validateMember = validateMember;
function randomId() {
    return ((Math.random().toString(36).substr(2, 5) + Date.now().toString(36) + Math.random().toString(36).substr(2, 5)));
}
const oAuth2Client = new google.google.auth.OAuth2(process.env.EMAIL_CLIENT_ID, process.env.EMAIL_CLIENT_SECRET, 'https://developers.google.com/oauthplayground');
oAuth2Client.setCredentials({
    refresh_token: process.env.EMAIL_REFRESH_TOKEN
});
function send_invite_mails(date, database) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const accessToken = yield oAuth2Client.getAccessToken();
            let url = "";
            const mail = fs.readFileSync('msg', 'utf8');
            const mailHtml = fs.readFileSync('msg_html', 'utf8');
            const transport = nodemailer.createTransport({
                // @ts-ignore
                service: "gmail",
                auth: {
                    type: "OAuth2",
                    user: process.env.EMAIL,
                    clientId: process.env.EMAIL_CLIENT_ID,
                    clientSecret: process.env.EMAIL_CLIENT_SECRET,
                    refreshToken: process.env.EMAIL_REFRESH_TOKEN,
                    accessToken: accessToken,
                }
            });
            let mails = yield database.knex
                .select("email", "id", "name")
                .from('users')
                .where("deleted_at", "is", null);
            url = process.env.FRONT_SERVER_PATH;
            for (let user of mails) {
                let pass = randomId();
                let mailOptions = {
                    from: process.env.EMAIL,
                    to: user.email,
                    subject: 'Wigilia WRS FTIMS',
                    text: mail.replace("<!---->", url),
                    html: mailHtml.replace("<!---->", url + "?uniq_code=" + pass + "&email=" + user.email).replace("<!----->", url),
                };
                const result = yield transport.sendMail(mailOptions);
                console.log(result);
                yield database.knex("users")
                    .update({
                    write_link: yield bcrypt_1.hashSync(pass, 6)
                })
                    .where('id', user.id);
            }
        }
        catch (exc) {
            console.log(exc);
        }
    });
}
exports.send_invite_mails = send_invite_mails;
