"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteMessage = void 0;
const utils_1 = require("./utils");
const constants_1 = require("./constants");
function deleteMessageDataBase(database, message, sender, receiver) {
    return __awaiter(this, void 0, void 0, function* () {
        let isRelationships;
        if (receiver) {
            isRelationships = yield database.knex
                .select('id')
                .from('messages')
                .where('sender_id', sender)
                .andWhere('deleted_at', 'is', null)
                .andWhere('receiver_id', receiver);
        }
        else {
            isRelationships = yield database.knex
                .select('id')
                .from('messages')
                .where('sender_id', sender)
                .andWhere('receiver_id', 'is', null)
                .andWhere('deleted_at', 'is', null);
        }
        if (isRelationships.length == 1) {
            yield database.knex('messages')
                .update({
                updated_at: new Date(),
                deleted_at: new Date(),
            })
                .where("id", isRelationships[0].id);
        }
        else
            return false;
        return true;
    });
}
function deleteMessage(app, database) {
    app.post('/deleteMessage', function (req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            if (constants_1.START_READING > new Date()) {
                yield utils_1.validateDelete(req.body, database).then((users) => __awaiter(this, void 0, void 0, function* () {
                    if (users.length != 0) {
                        yield deleteMessageDataBase(database, req.body.message, users[0].id, users[1] ? users[1].id : undefined)
                            .then((isDeleted) => {
                            if (isDeleted)
                                res.sendStatus(200);
                            else
                                res.sendStatus(400);
                        })
                            .catch(() => {
                            res.sendStatus(400);
                        });
                    }
                    else
                        res.sendStatus(400);
                })).catch(() => {
                    res.sendStatus(500);
                });
            }
            else {
                res.sendStatus(404);
            }
        });
    });
}
exports.deleteMessage = deleteMessage;
