"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.listOfMembers = void 0;
const utils_1 = require("./utils");
const constants_1 = require("./constants");
function listOfMembersDataBase(database, userId) {
    return __awaiter(this, void 0, void 0, function* () {
        let members = yield database.knex
            .select('name', 'email')
            .from('users')
            .where('id', '!=', userId)
            .andWhere('deleted_at', 'is', null);
        return members;
    });
}
function listOfMembers(app, database) {
    app.post('/listOfMembers', function (req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            if (constants_1.START_READING > new Date()) {
                yield utils_1.validateMember(req.body, database).then((userId) => __awaiter(this, void 0, void 0, function* () {
                    if (userId) {
                        yield listOfMembersDataBase(database, userId)
                            .then((members) => {
                            res.send(members).status(200);
                        })
                            .catch(() => {
                            res.sendStatus(400);
                        });
                    }
                    else
                        res.sendStatus(400);
                })).catch((err) => {
                    res.sendStatus(500);
                });
            }
            else {
                res.sendStatus(404);
            }
        });
    });
}
exports.listOfMembers = listOfMembers;
