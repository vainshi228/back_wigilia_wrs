"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createMessage_1 = require("./createMessage");
const readMessages_1 = require("./readMessages");
const updateMessage_1 = require("./updateMessage");
const deleteMessage_1 = require("./deleteMessage");
const listOfMembers_1 = require("./listOfMembers");
const getMyMessages_1 = require("./getMyMessages");
exports.default = {
    getMyMessages: getMyMessages_1.getMyMessages,
    listOfMembers: listOfMembers_1.listOfMembers,
    createMessage: createMessage_1.createMessage,
    readMessage: readMessages_1.readMessage,
    updateMessage: updateMessage_1.updateMessage,
    deleteMessage: deleteMessage_1.deleteMessage
};
