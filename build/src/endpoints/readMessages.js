"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.readMessage = void 0;
const utils_1 = require("./utils");
function readMessageDataBase(database, userId) {
    return __awaiter(this, void 0, void 0, function* () {
        let messages = yield database.knex
            .select('message')
            .from('messages')
            .where('deleted_at', 'is', null)
            .andWhere('receiver_id', 'is', null);
        messages.forEach((message) => { message.is_private = false; });
        let private_messages = yield database.knex
            .select('message')
            .from('messages')
            .where('deleted_at', 'is', null)
            .andWhere('receiver_id', userId);
        private_messages.forEach((message) => { message.is_private = true; });
        return messages.concat(private_messages);
    });
}
function readMessage(app, database) {
    app.post('/readMessage', function (req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //if (START_READING < new Date()) {
            yield utils_1.validateRead(database, req.body.uniq_code).then((userId) => __awaiter(this, void 0, void 0, function* () {
                if (userId) {
                    yield readMessageDataBase(database, userId)
                        .then((messages) => {
                        res.send(messages).status(200);
                    })
                        .catch(() => {
                        res.sendStatus(400);
                    });
                }
                else
                    res.sendStatus(400);
            })).catch((err) => {
                res.sendStatus(500);
            });
            //} else res.sendStatus(404);
        });
    });
}
exports.readMessage = readMessage;
