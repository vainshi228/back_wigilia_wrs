"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createMessage = void 0;
const utils_1 = require("./utils");
const constants_1 = require("./constants");
function createMessageDataBase(database, message, sender, receiver) {
    return __awaiter(this, void 0, void 0, function* () {
        if (receiver) {
            const isRelationships = yield database.knex
                .select('id')
                .from('messages')
                .where('sender_id', sender)
                .andWhere('deleted_at', 'is', null)
                .andWhere('receiver_id', receiver);
            if (isRelationships.length == 0) {
                const result = yield database.knex('messages')
                    .insert({
                    message: message,
                    sender_id: sender,
                    receiver_id: receiver,
                    created_at: new Date(),
                    updated_at: new Date(),
                });
            }
            else
                return false;
        }
        else {
            const isRelationships = yield database.knex
                .select('id')
                .from('messages')
                .where('sender_id', sender)
                .andWhere('receiver_id', 'is', null)
                .andWhere('deleted_at', 'is', null);
            if (isRelationships.length == 0) {
                const result = yield database.knex('messages')
                    .insert({
                    message: message,
                    sender_id: sender,
                    created_at: new Date(),
                    updated_at: new Date(),
                });
            }
            else
                return false;
        }
        return true;
    });
}
function createMessage(app, database) {
    app.post('/createMessage', function (req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            if (constants_1.START_READING > new Date()) {
                yield utils_1.validateUpdateCreate(req.body, database).then((users) => __awaiter(this, void 0, void 0, function* () {
                    if (users.length != 0) {
                        yield createMessageDataBase(database, req.body.message, users[0].id, users[1] ? users[1].id : undefined)
                            .then((isAdded) => {
                            if (isAdded)
                                res.sendStatus(200);
                            else
                                res.sendStatus(400);
                        })
                            .catch(() => {
                            res.sendStatus(400);
                        });
                    }
                    else
                        res.sendStatus(400);
                })).catch(() => {
                    res.sendStatus(500);
                });
            }
            else {
                res.sendStatus(404);
            }
        });
    });
}
exports.createMessage = createMessage;
