"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMyMessages = void 0;
const utils_1 = require("./utils");
const constants_1 = require("./constants");
function getMyMessagesDataBase(database, userId) {
    return __awaiter(this, void 0, void 0, function* () {
        let messages = yield database.knex
            .select('messages.message', 'users.email as receiver_email', "users.name as receiver_name")
            .from('messages')
            .fullOuterJoin('users', function () {
            this.on("users.id", "=", "messages.receiver_id");
        })
            .where('messages.sender_id', userId)
            .andWhere('messages.deleted_at', 'is', null)
            .andWhere('users.deleted_at', 'is', null);
        messages.forEach((message) => {
            if (message.receiver_name) {
                message.is_private;
            }
            else {
                message.receiver_name = undefined;
                message.receiver_email = undefined;
            }
        });
        return messages;
    });
}
function getMyMessages(app, database) {
    app.post('/getMyMessages', function (req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            if (constants_1.START_READING > new Date()) {
                yield utils_1.validateMember(req.body, database).then((userId) => __awaiter(this, void 0, void 0, function* () {
                    if (userId) {
                        yield getMyMessagesDataBase(database, userId)
                            .then((messages) => {
                            res.send(messages).status(200);
                        })
                            .catch((err) => {
                            res.sendStatus(400);
                        });
                    }
                    else
                        res.sendStatus(400);
                })).catch((err) => {
                    res.sendStatus(500);
                });
            }
            else {
                res.sendStatus(404);
            }
        });
    });
}
exports.getMyMessages = getMyMessages;
